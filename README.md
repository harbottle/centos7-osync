# centos7-osync
[![build status](https://gitlab.com/harbottle/centos7-osync/badges/master/build.svg)](https://gitlab.com/harbottle/centos7-osync/pipelines)

Build osync RPM for CentOS 7 using GitLab CI.

%define name osync
%define version 1.2RC3
%define release 1%(/usr/lib/rpm/redhat/dist.sh)

Name:		        %{name}
Version:        %{version}
Release:        %{release}
Summary:        robust file synchronization tool
Group:		      Applications/File
License:        BSD
URL:            https://www.netpower.fr/osync
Source0:        https://github.com/deajan/%{name}/archive/v%{version}.tar.gz
Patch0:         systemd.patch
Requires:       rsync openssh-server bash wget inotify-tools
BuildArch: 	    noarch

%description
A robust two way (bidirectional) file sync script based on rsync with fault tolerance, time control and ACL synchronization.

%prep
%setup -q
%patch0 -p1

%install
mkdir -p ${RPM_BUILD_ROOT}%{_bindir}
mkdir -p ${RPM_BUILD_ROOT}%{_unitdir}
install -m 0755 osync.sh ${RPM_BUILD_ROOT}%{_bindir}
install -m 0755 osync-batch.sh ${RPM_BUILD_ROOT}%{_bindir}
install -m 0755 -d ${RPM_BUILD_ROOT}/etc/osync
install -m 0644 sync.conf.example ${RPM_BUILD_ROOT}/etc/osync
install -m 0755 osync-srv@.service ${RPM_BUILD_ROOT}%{_unitdir}

%files
%defattr(-,root,root)
%attr(755,root,root) %{_bindir}/osync.sh
%attr(755,root,root) %{_bindir}/osync-batch.sh
%attr(644,root,root) /etc/osync/sync.conf.example
%attr(755,root,root) %{_unitdir}/osync-srv@.service

%doc

%changelog
* Mon Feb 20 2017 Richard Grainger <grainger@gmail.com>
- Remove source download from spec file and simplify

* Wed Feb 15 2017 Richard Grainger <grainger@gmail.com>
- Update version
- Download source archive based on version
- Add inotify-tools as dependency
- Patch systemd service file

* Sun Dec 18 2016 Orsiris de Jong <ozy@netpower.fr>
- Add systemd / initV differentiation
- Make source autodownload work
- Disable all macros except install

* Tue Aug 30 2016 Orsiris de Jong <ozy@netpower.fr>
- Initial spec file
